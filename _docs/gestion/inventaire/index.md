---
title: Inventaire
layout: default
has_children: false
parent: Gestion du jardin
nav_order: 2
---

L'inventaire est une vision "à plat" des éléments de votre jardin.

Dans l'inventaire, vous pouvez réaliser toutes les actions disponibles sur la carte, à l'exception de la création de resources seules ou de parcelles (qui doivent être placées _géorgaphiquement_).

![Vue de l'inventaire](/assets/images/inventory-01-overview.png)

1. <span class="red-dot">1</span> Cliquez ici pour valider un enlèvement
2. <span class="red-dot">2</span> Cliquez ici pour valider une mise en
   place
3. <span class="red-dot">3</span> Resource **retirée**, dans une
   parcelle

