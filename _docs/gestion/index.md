---
title: Gestion du jardin
layout: default
has_children: true
has_toc: false
---

La gestion du jardin passe par les trois parties suivantes :
![Menu dans une carte](/assets/images/05-map-toolbar.png)

1. [Gerer les ressources sur la carte interactive](/docs/gestion/carte/index/)
2. [Afficher l'inventaire](/docs/gestion/inventaire/index/)
3. [Afficher les tâches à faire](/docs/gestion/taches/index/)

