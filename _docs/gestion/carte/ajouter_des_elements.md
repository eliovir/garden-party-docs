---
title: Ajouter des ressources
layout: default
parent: La carte
grand_parent: Gestion du jardin
nav_order: 1
---

{% include toc.md %}

Les différents modes d'ajout:

![Modes](/assets/images/05-map-garden-modes.png)

- <span class="red-dot">1</span> Placement de ressource seule
- <span class="red-dot">2</span> Placement de parcelles

## Créer des ressources seules

Les "ressources seules" sont parfaits pour représenter des arbres, des
buissons ou des plants requérant une certaine attention. Libre à vous de
vous organiser comme bon vous semble après avoir un peu expérimenté avec
l'outil...

1. Sélectionnez l'outil <span class="red-dot">1</span>
2. Sélectionnez ensuite une ressource à placer (certaines ressources ont
   des variantes, c'est visible avec une petite icône à droite du nom)
3. Cliquez sur la carte à l'emplacement de la ressource

![Placement de ressource seule](/assets/images/05-place-one-resource.gif)


## Créer des parcelles

Les parcelles sont parfaites pour les zones géographiques qui
contiennent plusieurs ressources (exemple: une parcelle de radis, une
parcelle de choux et de laitues, ...). Elles peuvent contenir un ou
plusieur ressources.

1. Sélectionnez un des outils de géométrie <span
   class="red-dot">2</span>
2. Sélectionnez une ressource à placer
3. Cliquez sur la carte pour placer les limites de la parcelle

![Placement de parcelle](/assets/images/05-place-one-patch.gif)

## Ajouter des ressources à une parcelle

1. Passez en mode "_Revue_" (l'icône en forme de curseur de souris)
2. Cliquez sur la parcelle concernée. Une _popup_ d'information apparait
   (si la popup est coupée par les limites de la carte, vous pouvez la
   déplacer)
3. Cliquez sur "Ajouter une ressource"
4. Dans la liste des ressources, cliquez sur la ressource que vous
   souhaitez ajouter

![Ajout de ressource à une parcelle existante](/assets/images/05-add-resource-to-patch.gif)
