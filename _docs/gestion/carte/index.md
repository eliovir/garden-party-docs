---
title: La carte
layout: default
parent: Gestion du jardin
has_children: true
nav_order: 1
---

Une fois une carte ouverte, la gestion du jardin passe principalement
par ces éléments de l'interface:

![Interface de la carte](/assets/images/05-map-garden-interface.png)

- <span class="red-dot">1</span> Mode _Revue_ : Permet de sélectionner
  des éléments placés sur la carte, d'en afficher les propriétés et de
  réaliser des actions
- <span class="red-dot">2</span> Mode _Édition_ : Permet de déplacer des
  éléments et de redimensionner les parcelles
- <span class="red-dot">3</span> Mode _Ajout_ : Permet d'ajouter des
  ressources et parcelles
- <span class="red-dot">4</span> Filtres de niveaux.

## Les éléments

Les "ressources" sont tout ce qui peut être ajouté sur la carte pour
être suivi: plantes et animaux (quoi qu'il n'y en ait pas encore dans la
base de données pour le moment)

Les ressources "seules" représentent un élément, comme par exemple un
arbre ou un buisson. Ils seront représentés sur la carte par un point.

Les parcelles sont des zones géométriques dans lesquelles il peut y
avoir plusieur ressources, comme un carré d'arômates, contenant
différentes plantes.

## Les niveaux

Les niveaux représentent les différentes strates que l'on peut retrouver
dans les concepts de forêt-jardin, du sous-sol (_rhizosphère_) aux
arbres les plus hauts (_canopée_). Sont aussi représentés la _strate
grimpante_ et un niveau sur lequel seront placés les animaux.

Toutes les ressources appartiennent à un niveau donné, et cliquer sur
les boutons de la zone <span class="red-dot">4</span> masquera ou non
les éléments correspondants de la carte.
