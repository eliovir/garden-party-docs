---
title: Réaliser des actions
layout: default
parent: La carte
grand_parent: Gestion du jardin
nav_order: 3
---

{% include toc.md %}

## Mettre en place

**Note**: Au stade courant de développement, cette action n'apparait pas
encore dans l'historique de la resource concernée, ni dans la liste des
tâches.

La **mise en place** est le fait de mettre une ressource dans votre jardin,
_en vrai_.

Lors de la mise en place d'une resource, vous devez sélectionner la date
effective ou prévue.

Lorsqu'une resource doit être mise en place dans le futur, il faudra
valider l'action le jour J à partir de la vue "_Inventaire_".

Pour mettre en place un élément :

1. Passez en mode "_Revue_"
2. Cliquez sur l'élément concerné sur la carte (quand la resource est
   dans une parcelle, cliquer sur la parcelle)
3. Dans la _popup_, cliquez sur "_Mettre en place_"
4. Sélectionnez une date
5. Validez en cliquant sur "Enregistrer"

![Implantation](/assets/images/07-set-implantation-date.gif)

## Réaliser/planifier des actions

Les actions telles qu'_arroser_, _tailler_, ... ne sont disponibles que
sur les éléments déjà mis en place.

Pour réaliser une action :

1. Passez en mode "_Revue_"
2. Cliquez sur l'élément concerné sur la carte (quand la resource est
   dans une parcelle, cliquer sur la parcelle)
3. Dans la _popup_, cliquez sur l'action souhaitée
4. Sélectionnez une date
5. Ajoutez vos notes (optionnel, sauf pour "_observer_")
6. Validez en cliquant sur "Enregistrer"

![Faire une action maintenant](/assets/images/07-add-action-now.gif)

## Afficher l'historique

Tous les éléments mis en place ont un historique d'actions. Pour
l'afficher :

1. Passez en mode "_Revue_"
2. Cliquez sur l'élément concerné sur la carte (quand la resource est
   dans une parcelle, cliquer sur la parcelle)
3. Dans la _popup_, cliquez sur le bouton en forme de carnet, tout à
   droite de la resource

![Afficher l'historique](/assets/images/07-show-history.gif)

## Retirer

**Note**: Au stade courant de développement, cette action n'apparait pas
encore dans l'historique de la resource concernée, ni dans la liste des
tâches.

**Retirer** un élément est le fait de l'enlever de votre jardin, _en vrai_
(arracher/couper/faucher/... Tout ce qui fait que l'élément n'est plus
là).

Un élément retiré n'est plus visible sur la carte, mais est toujours
disponible dans l'"_inventaire": vous aurez toujours accès à
l'historique de ses modifications.

Lorsque vous retirez un élément, vous devez choisir la date effective ou
prévue pour cette action.

Lorsqu'une resource doit être retirée dans le futur, il faudra valider
l'action le jour J à partir de la vue "_Inventaire_".

Pour retirer un élément :

1. Passez en mode "_Revue_"
2. Cliquez sur l'élément concerné sur la carte (quand la resource est
   dans une parcelle, cliquer sur la parcelle)
3. Dans la _popup_, cliquez sur "_Retirer_"
4. Sélectionnez une date
5. Validez en cliquant sur "Enregistrer"

