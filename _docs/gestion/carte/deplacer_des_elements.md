---
title: Modifier/supprimer des éléments
layout: default
parent: La carte
grand_parent: Gestion du jardin
nav_order: 2
---

{% include toc.md %}

## Déplacer des ressources seules

1. Passez en mode "_Édition_" (icône en forme de crayon).
2. Cliquez sur la ressource à déplacer. Elle se transforme en point bleu
3. Déplacez la ressource

<small>Voir l'animation de "Redimensionner des parcelles".</small>

## Redimensionner des parcelles

1. Passez en mode "_Édition_" (icône en forme de crayon).
2. Cliquez sur la parcelle à modifier. Elle se transforme en zone bleue
3. Déplacez les angles en faisant un _cliquer/déplacer_ dessus, ou
   ajoutez-en en faisant un _cliquer/déplacer_ sur les côtés

![Redimensionnement de parcelle](/assets/images/06-resize-patch.gif)

## Renommer des ressources ou parcelles

1. Passez en mode "_Revue_" (icône en forme de curseur de souris)
2. Cliquez sur la resource ou parcelle. Une popup s'affiche
3. Cliquez sur "Éditer"
4. Donnez un petit nom à l'élément et enregistrez les changements

![Renommage d'élément](/assets/images/06-rename-patch.gif)

## Supprimer des ressources ou parcelles

Attention, supprimer une parcelle ou des ressources seules n'est pas
réversible. Vous perdrez l'historique de l'élément. Voyez "
[Retirer une ressource](/docs/gestion/carte/realiser_des_actions#retirer)" si vous souhaitez garder l'élément.

1. Passez en mode "_Revue_" (icône en forme de curseur de souris)
2. Cliquez sur la ressource ou la parcelle à supprimer
3. Dans la _popup_, cliquez sur "_Détruire_"
4. Validez la suppression.

![Destruction d'élément](/assets/images/06-destroy-patch.gif)

Il est encore aussi possible, en mode "_Édition_", de sélectionner
plusieur élément en maintenant "Maj" lorsque vous cliquez, et de
supprimer l'ensemble des éléments sélectionnés via le bouton "_Delete X
selected patches_". Cette fonctionnalité sera sûrement supprimée un
jour, et elle **ne demande aucune confirmation**.
