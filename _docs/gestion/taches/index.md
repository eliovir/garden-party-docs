---
title: Tâches
layout: default
has_children: false
parent: Gestion du jardin
nav_order: 3
---

La liste des tâches reprend jour par jour, les actions planifiées.

Vous pouvez voir dessus les actions en retard et les actions à venir, et
vous pouvez aussi les terminer ou les supprimer si vous abandonnez
l'idée.

![Vue de l'inventaire](/assets/images/tasks-01-overview.png)

1. <span class="red-dot">1</span> tâches en retard
2. <span class="red-dot">2</span> taches à venir
