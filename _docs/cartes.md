---
title: Gestion des cartes
layout: default
nav_order: 3
---

{% include toc.md %}

## Créer une carte

Lorsque vous ouvrez l'application, une liste de carte est présentée
(vide au premier lancement).

1. Cliquez sur "Nouvelle carte"
   ![Liste de cartes vide](/assets/images/03-empty-map-list.png)

Garden Party permet de dessiner son jardin sur des cartes
[OpenStreetMap](https://openstreetmaps.org) ou sur des images que vous
ajoutez vous-même (plan, croquis, photo aérienne, ...)

### Créer une carte à partir d'une image

C'est le choix par défaut.

1. Remplissez le nom de la carte
2. Choisissez une image <span class="red-dot">1</span>
   ![Formulaire](/assets/images/04-new-map-from-image.png)
3. Cliquez sur la carte là ou se trouvera le centre de votre jardin
   (_vous pouvez zoomer avec la molette de la souris, et déplacer
   l'image en restant cliqué lors du déplacement_)
   ![Le centre est au bout de la flèche](/assets/images/04-new-map-from-image-center.png)
4. Cliquez sur le bouton "Enregistrez cette carte"

Patientez quelques secondes et vous devriez vous retrouver "dans la
carte".

### Créer une carte à partir d'OpenStreetMaps

1. Cliquez sur "OpenStreetMaps"
2. Sur la carte qui apparait, zoomez sur votre jardin
3. Cliquez au centre du jardin
4. Cliquez sur le bouton "Enregistrez cette carte".

Patientez quelques secondes et vous devriez vous retrouver dans la
carte.

## Supprimer une carte

1. Accédez à l'application
2. Dans la liste des cartes, cliquez sur la corbeille rouge à droite du
   nom de la carte que vous souhaitez supprimer
3. Validez la suppression dans la fenêtre de confirmation

La carte, ses ressources et leur historique seront supprimés sans
possibilité de revenir en arrière.
