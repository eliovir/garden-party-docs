---
title: Inscription et connexion
layout: default
nav_order: 2
---

{% include toc.md %}

## Créer son compte

1. Accédez à Garden Party.
2. Dans le menu principal, cliquez sur "_Connexion_" <span class='red-dot'>3</span>.
   ![Menu principal](/assets/images/01-main-menu.png)
3. Cliquez ensuite sur "_Inscription_" <span class='red-dot'>2</span>
   ![Interface de connexion](/assets/images/01-sign-in.png)
4. Remplissez le formulaire et validez.

Après votre inscription, vous devriez être connecté.e : les boutons
"_Compte_" et "_Déconnexion_" sont visibles <span class='red-dot'>1</span>
![Menu principal, une fois connecté.e](/assets/images/02-logged-in-main-menu.png)

## Se connecter

1. Accédez à Garden Party.
2. Dans le menu principal, cliquez sur "_Connection_" <span class='red-dot'>3</span>.
   ![Menu principal](/assets/images/01-main-menu.png)
3. Entrez vos identifiants et validez <span class='red-dot'>1</span>.
   ![Interface de connexion](/assets/images/01-sign-in.png)

vous devriez être connecté.e : les boutons "_Compte_" et "_Déconnexion_"
sont visibles <span class='red-dot'>1</span>.
![Menu principal, une fois connecté.e](/assets/images/02-logged-in-main-menu.png)

## Accéder à l'application

Une fois que vous avez créé votre compte et que vous êtes connecté.e,
accédez à l'application en cliquant sur "_App_" dans le menu principal
<span class='red-dot'>2</span>.
![Menu principal, une fois connecté.e](/assets/images/02-logged-in-main-menu.png)

L'application charge les données nécessaire à son fonctionnement, ce qui
peut prendre quelques secondes.
