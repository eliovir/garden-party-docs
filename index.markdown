---
title: Documentation utilisateur
layout: default
navigation: 1
---

![Logo](assets/logo.svg){: .logo}

**Garden Party** est une application destinée à aider les jardiniers en
herbe dans la gestion de leur jardins.

- [Application](https://garden-party.experimentslabs.com)
- [Sources](https://gitlab.com/experimentslabs/garden-party)
- [Sources de cette documentation](https://gitlab.com/experimentslabs/garden-party-docs)

## Documentation

- [Création de compte](/docs/inscription_et_connexion)
- [Gestion des cartes](/docs/cartes)
- [Gestion du jardin](/docs/gestion/index)

## Discussions

Si vous souhaitez être au courant des mises à jour ou simplement discuter des problèmes existants et des améliorations possibles, vous pouvez :

- [rejoindre la liste de discution](https://framalistes.org/sympa/subscribe/garden-party) (échanges par courriel),
- [rejoindre le salon Matrix](https://matrix.to/#/!fROsPDUgtYILajhMWg:matrix.org?via=matrix.org) (chat direct),
- ou nous contacter directement à l'addresse suivante : "gardenparty arobase experimentslabs point com"
